


class App {
  final name;
  final author;
  final category;
  final year;

  const App(this.name, this.author, this.category, this.year);

  void getAppName(String name, String author, 
                  String category, int year){
    print('$name was developed by $author and won MTNApp Award in $year for $category');
  } 
  
  bool isUppercased(String name){
  return name == name.toUpperCase();
   }

} 

 